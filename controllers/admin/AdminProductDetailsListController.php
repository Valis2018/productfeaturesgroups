<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
require_once _PS_MODULE_DIR_.'productfeaturesgroups/model/FeatureGroup.php';

class AdminProductDetailsListController extends AdminController
{
    public $id_group = null;
    public $object = null;
    public $product = null;
    public $redirect_after = false;
    public $module;

    public function __construct()
    {
        $this->context = Context::getContext();
        $this->module = Module::getInstanceByName('productfeaturesgroups');

        $id_product = (int)Tools::getValue('id_product');

        $this->product = new Product($id_product, false, $this->context->language->id);
        $this->bootstrap = true;
        $this->className = 'FeatureGroup';
        $this->table = 'pfg_group';
        $this->identifier = 'id_group';
        $this->explicitSelect = true;
        $this->lang = true;
        $this->default_form_language = $this->context->language->id;

        /**
         * Remove click on row
         */
        $this->list_no_link = true;

        $this->addRowAction('delete');
        $this->default_form_language = $this->context->language->id;
        $this->addRowAction('edit');
        $this->_use_found_rows = false;
        $this->fields_list = array(
            'position' => array(
                'title' => $this->module->l('Position'),
                'align' => 'text-center',
                'search' => true,
                'filter_type' => 'int',
                'filter_key' => 'a!position',
                'width' => 'auto',
            ),
            'id_group' => array(
                'title' => $this->module->l('ID'),
                'class' => 'fixed-width-xs',
                'search' => true,
                'filter_type' => 'int',
                'filter_key' => 'a!id_group',
                'align' => 'text-center',
            ),

            /**
             * Multi-lang field
             */
            'name' => array(
                'title' => $this->module->l('Name'),
                'align' => 'text-left',
                'havingFilter' => false,
                'filter_key' => 'gl!name',
                'width' => 'auto',
            ),
            'feature_ids' => array(
                'title' => $this->module->l('Features'),
                'width' => 'auto',
                'havingFilter' => false,
                'align' => 'text-left',
                'callback' => 'formatFeatures',
                'filter_key' => 'a!feature_ids',
            ),
            /**
             * Multi-lang field
             */
            'description' => array(
                'align' => 'text-left',
                'title' => $this->module->l('Description'),
                'havingFilter' => false,
                'filter_key' => 'gl!description',
                'width' => 'auto',
            ),
        );

        $this->object = new FeatureGroup($this->id_group);
        $this->_select = 'gl.`name`, gl.`description`';
        $this->_join = 'LEFT JOIN `' . _DB_PREFIX_ . 'pfg_group_lang` gl ON 
        (gl.`id_group` = a.`id_group` AND gl.`id_lang`="'
            . (int)$this->context->language->id . '")';
        $this->id_group = (int)Tools::getValue('id_group') ? (int)Tools::getValue('id_group') : null;
        $this->_orderWay = 'desc';

        $this->page_header_toolbar_title = $this->module->l('Group list for') . ': ' . $this->product->name;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function renderList()
    {
        $this->context->smarty->assign($this->module->assignConfigureLinks());
        return $this->context->smarty->fetch($this->module->getLocalPath() . 'views/templates/admin/menu.tpl') .
            parent::renderList();
    }


    /**
     * @param $data
     * @param $row
     * @return string|null
     * This function is calling the name of the features instead of IDs
     */
    public static function formatFeatures($data, $row)
    {
        static $features;

        if (!$features) {
            $idLang = Context::getContext()->language->id;
            $features = Db::getInstance()->executeS('
                SELECT `id_feature`, `name` FROM `' . _DB_PREFIX_ . 'feature_lang`
                WHERE `id_lang` = '. (int)$idLang);
        }

        $featureIds = json_decode($data, true) ?: array();

        if (empty($featureIds)) {
            return null;
        }

        $selectedFeatures = array_filter($features, function ($feature) use ($featureIds) {
            return in_array($feature['id_feature'], $featureIds);
        });

        $featureNames = array();
        foreach ($selectedFeatures as $feature) {
            $featureNames[] = $feature['name'];
        }

        return implode(', ', $featureNames);
    }

    /**
     * Get the feature names in the admin select
     */
    public function getFeaturesInfo()
    {
        $idLang = Context::getContext()->language->id;

        $features = Db::getInstance()->executeS('
                SELECT `id_feature`, `name` FROM `' . _DB_PREFIX_ . 'feature_lang`
                WHERE `id_lang` =' . (int)$idLang);
        $this->context->smarty->assign(array(
            'featureNames' => $features
        ));
    }

    public function getSelectedFeatures()
    {
        $idLang = Context::getContext()->language->id;
        $selectedFeatures = Db::getInstance()->getRow('
          SELECT `feature_ids` FROM `' . _DB_PREFIX_ . 'pfg_group`
          WHERE `id_group` = ' . (int)$this->object->id_group);

        $selectedFeaturesIds = json_decode($selectedFeatures['feature_ids'], true);
        $features = array();

        if ($selectedFeaturesIds) {
            $features = Db::getInstance()->executeS('
                    SELECT `id_feature`, `name` FROM `' . _DB_PREFIX_ . 'feature_lang`
                    WHERE `id_lang` =' . (int)$idLang . '
                    AND `id_feature` IN (' . pSQL(implode(',', $selectedFeaturesIds)) . ')');
        }

        $this->context->smarty->assign(array(
            'selectedFeatures' => $features,
        ));
    }

    /**
     * @return string
     * Add group form
     */
    public function renderForm()
    {
        self::$currentIndex;

        /**
         * count the group entries in the db
         */
        $countGroupEntries = Db::getInstance()->getValue('SELECT COUNT(id_group) FROM ' . _DB_PREFIX_ . 'pfg_group');
        $numberofGroups = range(1, (int)$countGroupEntries + 1);
        $numbers = array();
        foreach ($numberofGroups as $number) {
            $numbers[] = array(
                'id' => $number,
                'name' => $this->module->l('Position') . ' #' . $number
            );
        }
        $this->fields_form = array(
            'legend' => array(
                'title' => ($this->id_group) ? $this->module->l('Edit group') : $this->module->l('Add a new group'),
                'icon' => 'icon-plus-circle'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'desc' => $this->module->l('Insert the name of your group'),
                    'label' => $this->module->l('Group name'),
                    'required' => true,
                    'name' => 'name',
                    'col' => '4',
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->module->l('Group description'),
                    'desc' => $this->module->l('Insert a short description for this group. It will be visible in the front office.'),
                    'name' => 'description',
                    'hint' => $this->module->l('This is optional'),
                    'lang' => true,
                    'col' => '4',
                ),
                array(
                    'type' => 'select',
                    'label' => $this->module->l('Group position'),
                    'desc' => $this->module->l('Select the position of the group in the front office'),
                    'hint' => $this->module->l('If you select the same position for 2 or more groups nothing will happen.'),
                    'name' => 'position',
                    'col' => '4',
                    'options' => array(
                        'query' => $numbers,
                        'id' => 'id',
                        'name' => 'name',
                    )
                ),
                array(
                    'type' => 'selectfeatures',
                    'name' => 'selected_features',

                ),
                array(
                    'type' => 'text',
                    'label' => $this->module->l('Product IDs'),
                    'desc' => $this->module->l('Insert product IDs separated by commas. Ex: 1, 4, 7, 25, 60 etc.'),
                    'hint' => $this->module->l('Insert the product IDs for which this group will be displayed on the front office'),
                    'name' => 'productids',
                    'required' => true,
                    'col' => '6',
                ),
            ),
        );

        $this->fields_form['submit'] = array(
            'title' => $this->module->l('Save'),
        );

        $this->getFeaturesInfo();
        $this->getSelectedFeatures();
        
        return $this->renderFormMain();
    }

    /**
     * Function used to render the form for this controller*
     * @return string
     * @throws Exception
     * @throws SmartyException
     */
    public function renderFormMain()
    {
        if (!$this->default_form_language) {
            $this->getLanguages();
        }

        if (Tools::getValue('submitFormAjax')) {
            $this->content .= $this->context->smarty->fetch('form_submit_ajax.tpl');
        }

        if ($this->fields_form && is_array($this->fields_form)) {
            if (!$this->multiple_fieldsets) {
                $this->fields_form = array(array('form' => $this->fields_form));
            }

            /**
             * For add a fields via an override of $fields_form, use $fields_form_override
             */
            if (is_array($this->fields_form_override) && !empty($this->fields_form_override)) {
                $this->fields_form[0]['form']['input'] = array_merge(
                    $this->fields_form[0]['form']['input'],
                    $this->fields_form_override
                );
            }

            $fields_value = $this->getFieldsValue($this->object);

            Hook::exec('action' . $this->controller_name . 'FormModifier', array(
                'fields' => &$this->fields_form,
                'object' => &$this->object,
                'form_vars' => &$this->tpl_form_vars,
                'fields_value' => &$fields_value,
            ));
            
            
            $helper = new HelperForm($this);
            $this->setHelperDisplay($helper);
            $helper->fields_value = $fields_value;
            $helper->submit_action = $this->submit_action;
            $helper->tpl_vars = $this->getTemplateFormVars();
            $helper->show_cancel_button = (isset($this->show_form_cancel_button)) ? $this->show_form_cancel_button :
                ($this->display == 'add' || $this->display == 'edit');
            $helper->override_folder = null;
            $helper->module = $this->module;


            $back = Tools::safeOutput(Tools::getValue('back', ''));
            if (empty($back)) {
                $back = self::$currentIndex . '&token=' . $this->token;
            }
            if (!Validate::isCleanHtml($back)) {
                die(Tools::displayError());
            }

            /**
             * Cancel button
             */
            if (Tools::version_compare(_PS_VERSION_, '1.7', '>=')) {
                $helper->back_url = $back;
                !is_null($this->base_tpl_form) ? $helper->base_tpl = $this->base_tpl_form : '';
                if ($this->access('view')) {
                    if (Tools::getValue('back')) {
                        $helper->tpl_vars['back'] = Tools::safeOutput(Tools::getValue('back'));
                    } else {
                        $helper->tpl_vars['back'] = Tools::safeOutput(Tools::getValue(self::$currentIndex . '&token=' .
                            $this->token));
                    }
                }
            }

            $helper->tpl_vars['uri'] = $this->module->getPathUri();

            $form = $helper->generateForm($this->fields_form);

            return $form;
        }
        return $this->renderFormMain();
    }

    /**
     * Init Toolbar function
     */
    public function initToolbar()
    {
        parent::initToolbar();

        if (isset($this->toolbar_btn['new'])) {
            $this->toolbar_btn['new']['href'];
        }
    }

    /**
     * @param $object
     * @return mixed
     */
    public function getFieldsValue($object)
    {
        if (Validate::isLoadedObject($this->object)) {
            $this->fields_value['productids'] = implode(',', $this->getProductIds($this->object->id));
        }

        return parent::getFieldsValue($object);
    }

    /**
     * The adding process
     */
    public function processAdd()
    {
        $productIds = Tools::getValue('productids');
        $productIds = array_map('intval', $productIds);
        $productIds = explode(',', $productIds);
        $productIds = array_unique($productIds);
        $productIds = array_filter($productIds, function ($productId) {
            return $productId !== 0;
        });

        if (!Tools::getValue('productids')) {
            $this->errors[] = $this->module->l('Insert at least a product ID');
        }
        if (!Tools::getValue('feature_ids')) {
            $this->errors[] = $this->module->l('Please select some features');
        }

        parent::processAdd();
        $this->addProductIds((int)$this->object->id, $productIds);

        /**
         * Confirmation at redirect for helper list
         */
        if (!sizeof($this->errors)) {
            if (Tools::version_compare(_PS_VERSION_, "1.7", ">=")) {
                Tools::redirectAdmin($this->context->link->getAdminLink(
                    'AdminProductDetailsList',
                    true,
                    array(),
                    array('conf' => 4)
                ));
            } else {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminProductDetailsList').'&conf=4');
            }
        }
    }

    public function processUpdate()
    {
        $productIds = Tools::getValue('productids');
        $productIds = explode(',', $productIds);
        $productIds = array_unique($productIds);
        $productIds = array_filter($productIds, function ($productId) {
            return $productId !== 0;
        });
        $productIds = array_map('intval', $productIds);

        if (!Tools::getValue('productids')) {
            $this->errors[] = $this->module->l('Insert at least a product ID');
        }
        if (!Tools::getValue('feature_ids')) {
            $this->errors[] = $this->module->l('Please select some features');
        }
        parent::processUpdate();

        $this->addProductIds((int)$this->object->id, $productIds);

        /**
         * Confirmation at redirect to helper list
         */
        if (!sizeof($this->errors)) {
            if (Tools::version_compare(_PS_VERSION_, "1.7", ">=")) {
                Tools::redirectAdmin($this->context->link->getAdminLink(
                    'AdminProductDetailsList',
                    true,
                    array(),
                    array('conf' => 4)
                ));
            } else {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminProductDetailsList').'&conf=4');
            }
        }
    }

    /**
     * @param $idGroup
     * @param $idProducts
     */
    public function addProductIds($idGroup, $idProducts)
    {
        $table = _DB_PREFIX_.'pfg_group_product';
        $db = Db::getInstance();

        $db->execute("delete from `". pSQL($table) ."` where id_group = ". (int)$idGroup);

        foreach ($idProducts as $idProduct) {
            $db->execute("insert into `". pSQL($table) ."` (id_group, id_product) values(".(int)$idGroup. ",". (int)$idProduct .")");
        }
    }

    /**
     * @param $idGroup
     * @return array
     */
    public function getProductIds($idGroup)
    {
        $table = _DB_PREFIX_.'pfg_group_product';
        $db = Db::getInstance();

        $results = $db->executeS("select id_product from `". pSQL($table) ."` where id_group = ".(int)$idGroup);

        if (! $results) {
            return array();
        }

        $ids = array();
        foreach ($results as $row) {
            $ids[] = (int)$row['id_product'];
        }
        return $ids;
    }
}