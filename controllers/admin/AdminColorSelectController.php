<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class AdminColorSelectController extends AdminController
{
    public $module;

    /**
     * @var string
     * The controller that has to render
     */
    public $display = 'edit';
    public $submit_action = false;


    public $colors = array(
        'PFG_GROUP_TITLE' => '#7a7a7a',
        'PFG_GROUP_BACKGROUND' => '#f2f4f7',
        'PFG_GROUP_DESCRIPTION' => '#7a7a7a',
        'PFG_FEATURE' => '#7a7a7a',
        'PFG_FEATURE_DETAIL' => '#7a7a7a',

    );

    protected $fields = array(
        'PFG_GROUP_TITLE',
        'PFG_GROUP_BACKGROUND',
        'PFG_GROUP_DESCRIPTION',
        'PFG_FEATURE',
        'PFG_FEATURE_DETAIL',
    );

    /**
     * AdminColorSelectController constructor.
     */
    public function __construct()
    {
        $this->module = Module::getInstanceByName('productfeaturesgroups');
        $this->bootstrap = true;
        parent::__construct();
    }

    /**
     * @return string
     */
    public function renderForm()
    {
        $this->fields_form = array(
            'legend' => array(
                'icon' => 'icon-tint',
                'title' => $this->module->l('Pick your colors'),
            ),
            'input' => array(
                array(
                    'type' => 'color',
                    'desc' => $this->module->l('Set the title color for your groups'),
                    'label' => $this->module->l('Set the text color of your groups titles'),
                    'name' => 'PFG_GROUP_TITLE',
                ),
                array(
                    'type' => 'color',
                    'desc' => $this->module->l('Set the color for your groups description'),
                    'name' => 'PFG_GROUP_DESCRIPTION',
                    'label' => $this->module->l('Set the text color of your groups text description')
                ),
                array(
                    'type' => 'color',
                    'desc' => $this->module->l('Set the colors for your features'),
                    'name' => 'PFG_FEATURE',
                    'label' => $this->module->l('Select a font color for your features'),
                ),
                array(
                    'desc' => $this->module->l('Set the color for your feature details'),
                    'type' => 'color',
                    'label' => $this->module->l('Select a font color for your feature details'),
                    'name' => 'PFG_FEATURE_DETAIL',
                ),
                array(
                    'desc' => $this->module->l('Set the features background color'),
                    'type' => 'color',
                    'label' => $this->module->l('Select a color for your feature background'),
                    'name' => 'PFG_GROUP_BACKGROUND',
                ),
                array(
                    'name' => 'PFG_RESET_COLORS',
                    'type' => 'switch',
                    'label' => $this->module->l('Reset to default colors on save'),
                    'desc' => $this->module->l('Reset to default colors on save'),
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'value' => true,
                            'id' => 'active_on',
                            'label' => $this->module->l('Activate')
                        ),
                        array(
                            'id' => 'active_off',
                            'label' => $this->module->l('Deactivate'),
                            'value' => false
                        )
                    ),
                ),
            )
        );

        $this->fields_form['submit'] = array(
            'name' => 'saveColors',
            'title' => $this->module->l('Save'),
        );

        foreach ($this->fields as $field) {
            $this->tpl_form_vars['fields_value'][$field] = Configuration::get($field);
        }

        $this->tpl_form_vars['fields_value']['PFG_RESET_COLORS'] = 0;
        $menu = $this->context->smarty->fetch($this->module->getLocalPath().'views/templates/admin/menu.tpl');
        $this->context->smarty->assign($this->module->assignConfigureLinks());

        return $this->postProcess() . $menu . parent::renderForm();
    }

    /**
     * @return bool|ObjectModel|string
     */
    public function postProcess()
    {
        if (Tools::getIsset('saveColors')) {
            if (Tools::getValue('PFG_RESET_COLORS')) {
                // reset the colors
                foreach ($this->colors as $name => $default) {
                    if (!Configuration::updateValue($name, $default)) {
                        return $this->module->displayError($this->module->l('Something went wrong. Please try again.'));
                    }
                    $this->confirmations[] = $this->module->l('Settings saved!');
                }
            } else {
                foreach ($this->fields as $field) {
                    if (! preg_match("/#([a-f0-9]{3}){1,2}\b/i", Tools::getValue($field))) {
                        $this->errors[] = $this->module->l('Invalid color.');
                    }
                }
                if (count($this->errors) == 0) {
                    foreach ($this->fields as $field) {
                        if (!Configuration::updateValue($field, Tools::getValue($field))) {
                            return $this->errors[] = $this->module->l('Something went wrong. 
                            Please try again.');
                        }
                    }
                    $this->confirmations[] = $this->module->l('Settings saved!');
                }
            }
        }
    }
}
