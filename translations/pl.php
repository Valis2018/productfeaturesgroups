<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{productfeaturesgroups}prestashop>productfeaturesgroups_6e8b712ec5861a7c9c35a807843cbdf6'] = 'Product Feature Groups PRO';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_5a179f3f0bb807167d9f2a85016dc037'] = 'Wybierz swoje kolory';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_2a6880c29003c585476442e457590ecc'] = 'Ustaw kolor tytułu dla swoich grup';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_06480750638358e36dc7e8534f095fa8'] = 'Ustaw kolor tekstu tytułów grup';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_fd0a25c238e20c3b86a9378db4fc1e57'] = 'Ustaw kolor opisu grupy';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_5dd9b9bc63bdd2fdeb80fad3f45fae80'] = 'Ustaw kolor tekstu opisu tekstów grup';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_9cd4ca333a1b141d29c5d7883211ff8e'] = 'Ustaw kolory swoich funkcji';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_f39aadb2b605b2c0ef0a913d159fb7e8'] = 'Wybierz kolor czcionki dla swoich funkcji';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_4b513717b4d2c8bf4c147dbbbebce5c0'] = 'Ustaw kolor dla szczegółów funkcji';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_7599c9c05ca3e0e1427e7aa9048981a0'] = 'Wybierz kolor czcionki dla szczegółów funkcji';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_01fa641108c0d2452b411e60cb6a5133'] = 'Ustaw kolor tła grup';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_4fbf267ce09f22df9049c3e32d266e31'] = 'Wybierz kolor tła grup elementów';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_c8840ad0b3896ddb7aa0c925c6cf3392'] = 'Przywróć domyślne kolory przy zapisie';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_a13367a8e2a3f3bf4f3409079e3fdf87'] = 'Aktywuj';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_109fec06829bd79d222cfc8af52aaaf1'] = 'Dezaktywować';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_c9cc8cce247e49bae79f15173ce97354'] = 'Zapisać';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_e9f36d65b0491f99ef8f0ce2c11bf998'] = 'Coś poszło nie tak. Proszę spróbuj ponownie.';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_519928d26756f485aa6dde0b4023a003'] = 'Ustawienia zapisane!';
$_MODULE['<{productfeaturesgroups}prestashop>admincolorselectcontroller_d622350b75076ac2265a087a3447b5c2'] = 'Nieprawidłowy kolor.';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_b718adec73e04ce3ec720dd11a06a308'] = ' ID';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_52f5e0bc3859bc5f5e25130b6c7e8881'] = 'Pozycja';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_49ee3087348e8d44e1feda1917443987'] = 'Nazwa';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_98f770b0af18ca763421bac22b4b6805'] = 'Cechy';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Opis';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_ac5492d25481876175a763d920d293f6'] = 'Lista grup dla';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_3bd11b19e48893c0e79521359ee42211'] = 'Edytuj grupę';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_e1f67764d44c555e05d7fbacc1e9469d'] = 'Dodaj nową grupę';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_c6155aaecccf794cd2a00fcc35898022'] = 'Nazwa grupy';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_0376c0f40c10df01cad344d7fe7f3f8a'] = 'Wstaw nazwę grupy';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_3f435aacbc2371a5fa086f0996bbe6d4'] = 'Opis grupy';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_6ac0f11d69aa10422da6d768d3d2e23d'] = 'Wstaw krótki opis dla tej grupy. Będzie to widoczne w biurze.';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_b5c0417c31cab016a312912d330f7a10'] = 'Jest to opcjonalne';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_471ff610b7722a211a4d34c8d8564b4b'] = 'Pozycja grupy';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_b53bf377a69103c72600bd7978cfff54'] = 'Wybierz pozycję grupy w biurze głównym';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_36be9262e07971919c95b416a7743422'] = 'Jeśli wybierzesz tę samą pozycję dla 2 lub więcej grup nic się nie stanie.';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_4241591ecae342ac08eaad6776a9a2a6'] = 'Identyfikatory produktów';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_2fcbb8ef34cc4783d65e2df6a55f3bde'] = 'Wstaw identyfikator produktu oddzielony przecinkami. Przykład: 1, 4, 7, 25, 60 itd.';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_0baffd0147f855a7708e55f52136a2f3'] = 'Wprowadź identyfikatory produktów, dla których ta grupa będzie wyświetlana w biurze głównym';
$_MODULE['<{productfeaturesgroups}prestashop>adminproductdetailslistcontroller_c9cc8cce247e49bae79f15173ce97354'] = 'Zapisać';
$_MODULE['<{productfeaturesgroups}prestashop>menu_eaba3ed7e8193f98594565ce30b2f0a6'] = ' Zawiera listę grup';
$_MODULE['<{productfeaturesgroups}prestashop>menu_5d00e26ee8b2b454cc0a97349c126574'] = 'Otwórz katalog (nowa karta)';
$_MODULE['<{productfeaturesgroups}prestashop>menu_4cffe0c73b1b007ea90cd5a5334322ba'] = 'Wybierz kolory';
$_MODULE['<{productfeaturesgroups}prestashop>form_c0263ecd7daf17fecedaeb70e4a84623'] = 'Funkcje związane z tą grupą:';
$_MODULE['<{productfeaturesgroups}prestashop>form_d356dc57bb6229fbf0ad48a563f89682'] = 'Wybierz funkcje:';
$_MODULE['<{productfeaturesgroups}prestashop>form_1063e38cb53d94d386f21227fcd84717'] = 'Usunąć';
$_MODULE['<{productfeaturesgroups}prestashop>form_ec211f7c20af43e742bf2570c3cb84f9'] = 'Dodaj';
$_MODULE['<{productfeaturesgroups}prestashop>front_b8900d8adf598b6653bb8c071bc49a1d'] = 'Szczegóły Produktu';
