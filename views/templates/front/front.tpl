{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $prestashop17}
<div class="tabs" id="customtabs" >
        <ul class="nav nav-tabs" role="tablist">
            {if $product.description}
                <li class="nav-item">
                    <a
                        class="nav-link{if $product.description|escape:'htmlall':'UTF-8'} active{/if}"
                        data-toggle="tab"
                        href="#description_copy"
                        role="tab"
                        aria-controls="description"
                        {if $product.description|escape:'htmlall':'UTF-8'} aria-selected="true"{/if}>{l s='Description' d='Shop.Theme.Catalog'}</a>
                </li>
            {/if}
            {if $___groups}
            <li class="nav-item">
                <a
                    class="nav-link{if !$product.description} active{/if}"
                    data-toggle="tab"
                    href="#product-details_copy"
                    role="tab"
                    aria-controls="product-details"
                    {if !$product.description} aria-selected="true"{/if}>{l s='Product Details' d='Shop.Theme.Catalog'}</a>
            </li>
            {/if}
            {if $product.attachments}
                <li class="nav-item">
                    <a
                        class="nav-link"
                        data-toggle="tab"
                        href="#attachments"
                        role="tab"
                        aria-controls="attachments">{l s='Attachments' d='Shop.Theme.Catalog'}</a>
                </li>
            {/if}
            {foreach from=$product.extraContent item=extra key=extraKey}
                <li class="nav-item">
                    <a
                        class="nav-link"
                        data-toggle="tab"
                        href="#extra-{$extraKey|escape:'htmlall':'UTF-8'}"
                        role="tab"
                        aria-controls="extra-{$extraKey|escape:'htmlall':'UTF-8'}">{$extra.title|escape:'htmlall':'UTF-8'}</a>
                </li>
            {/foreach}
        </ul>

        <div class="tab-content" id="tab-content">
            <div class="tab-pane fade in{if $product.description} active{/if}" id="description_copy" role="tabpanel">
                {block name='product_description'}
                    <div class="product-description">{$product.description nofilter} {* This is HTML content *}</div>
                {/block}
            </div>

            {block name='product_details'}
                <div class="tab-pane fade{if !$product.description} in active{/if}"
                     id="product-details_copy"
                     data-product="{$product.embedded_attributes|json_encode}"
                     role="tabpanel">

                    {*Here comes the data*}
                    {foreach $___groups as $group}
                    {if ! empty($group.features)}
                    <button class="accordion group-title" style="color:{$PFG_GROUP_TITLE|escape:'htmlall':'UTF-8'}; background:{$PFG_GROUP_BACKGROUND|escape:'htmlall':'UTF-8'};">{$group.name|escape:'htmlall':'UTF-8'}</button>
                            <div class="panel">
                        <div class="group-description" style="color:{$PFG_GROUP_DESCRIPTION|escape:'htmlall':'UTF-8'}">{$group.description|escape:'htmlall':'UTF-8'}</div>

                            <table class="table table-sm table-striped features-table">
                                <tbody>
                                {foreach $group.features as $feature}
                                <tr>
                                    <td width="20%" class="feature-name" style="color:{$PFG_FEATURE|escape:'htmlall':'UTF-8'};"><b>{$feature.name|escape:'htmlall':'UTF-8'}:</b></td>
                                    <td class="feature-itself" style="color:{$PFG_FEATURE_DETAIL|escape:'htmlall':'UTF-8'};">{$feature.value|escape:'htmlall':'UTF-8'}</td>
                                </tr>
                                {/foreach}
                                </tbody>
                            </table>
                    </div>
                        {/if}
                    {/foreach}
                </div>
            {/block}
            {foreach from=$product.extraContent item=extra key=extraKey}
            <div class="tab-pane fade in {$extra.attr.class|escape:'htmlall':'UTF-8'}" id="extra-{$extraKey|escape:'htmlall':'UTF-8'}" role="tabpanel" {foreach $extra.attr as $key => $val} {$key}="{$val}"{/foreach}>
            {$extra.content nofilter} {* This is HTML content *}
        </div>
        {/foreach}
    </div>

    {else}
    {if $___groups}
    <section class="page-product-box">
    <h3 class="page-product-heading">{l s='Product Details' mod='productfeaturesgroups'}</h3>

        {foreach $___groups as $group}
            {if ! empty($group.features)}
                <button class="accordion group-title" style="color:{$PFG_GROUP_TITLE|escape:'htmlall':'UTF-8'}; background:{$PFG_GROUP_BACKGROUND|escape:'htmlall':'UTF-8'};">{$group.name|escape:'htmlall':'UTF-8'}</button>
                <div class="panel">
                    <div class="group-description" style="color:{$PFG_GROUP_DESCRIPTION|escape:'htmlall':'UTF-8'}">{$group.description|escape:'htmlall':'UTF-8'}</div>

                    <table class="table table-sm table-striped features-table">
                        <tbody>
                        {foreach $group.features as $feature}
                            <tr>
                                <td width="20%" class="feature-name" style="color:{$PFG_FEATURE|escape:'htmlall':'UTF-8'};"><b>{$feature.name|escape:'htmlall':'UTF-8'}:</b></td>
                                <td class="feature-itself" style="color:{$PFG_FEATURE_DETAIL|escape:'htmlall':'UTF-8'};">{$feature.value|escape:'htmlall':'UTF-8'}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            {/if}
        {/foreach}
    </section>
    {/if}
{/if}




