{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div align="center" style="margin-bottom: 3px;">
    <a href='{$pfg_documentation|escape:'htmlall':'UTF-8'}' class='btn btn-default btn-lg'><span class="icon-list"></span> {l s='Features Groups List' mod='productfeaturesgroups'}</a>
    <a href='{$pfg_back_to_catalog|escape:'htmlall':'UTF-8'}' class='btn btn-default btn-lg' target="_blank"><span class="icon-book"></span> {l s='Open Catalog (new tab)' mod='productfeaturesgroups'}</a>
    <a href='{$pfg_colors|escape:'htmlall':'UTF-8'}' class='btn btn-default btn-lg'><span class="icon-tint"></span> {l s='Select Colors' mod='productfeaturesgroups'}</a>
</div>
