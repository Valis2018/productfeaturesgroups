{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/form/form.tpl"}

{block name="field"}
    {if $input.type == 'selectfeatures'}
        <div class="form-group row">
            <label class="control-label col-lg-3"> </label>
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-4">
                        <h4 style="margin-top:5px;"><span style="color: red;">*</span> {l s='Features associated to this group:' mod='productfeaturesgroups'} </h4>
                        <select id="fields" multiple="multiple" name="feature_ids[]" style="width: 300px; height: 120px;">
                        {foreach $selectedFeatures as $selectedFeature}
                            <option value="{$selectedFeature.id_feature|escape:'htmlall':'UTF-8'}">
                                {$selectedFeature.name|escape:'htmlall':'UTF-8'}
                            </option>
                        {/foreach}
                        </select>
                    </div>

                    <div class="col-lg-4">
                        <h4 style="margin-top:5px;">{l s='Select features:' mod='productfeaturesgroups'}</h4>
                        <select id="availableFields" multiple="multiple" style="width: 300px; height: 120px;">
                            {foreach $featureNames as $featureName}
                                <option value="{$featureName.id_feature|escape:'htmlall':'UTF-8'}" selected>
                                {$featureName.name|escape:'htmlall':'UTF-8'} (id:{$featureName.id_feature|escape:'htmlall':'UTF-8'})
                                </option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-4">
                        <a id="removeField" class="btn btn-default" href="#">
                            <i class="icon-trash"></i>
                            {l s='Remove' mod='productfeaturesgroups'}
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a id="addField" class="btn btn-default" href="#">
                            <i class="icon-arrow-left"></i>
                            {l s='Add' mod='productfeaturesgroups'}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    {else}
        {$smarty.block.parent}
    {/if}
{/block}