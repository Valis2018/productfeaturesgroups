/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

$(document).ready(function(){
    setTimeout(function(){
        $('#fields option').each(function(){ $(this).attr('selected','selected');});
    },100);
});

console.log('here I am');

$(function(){
    var  fields_left = $("#items"),
        fields_right = $("#availableItems"),
        remove_fields = $("#removeItem"),
        add_fields = $("#addItem");

    /**
     * ADD categories event
     */
    add_fields.click(function(e){
        var selected_fields = fields_right.find("option:selected");
        selected_fields.each(function(key, option){
            var clone = $(option).clone();
            var re = new RegExp(String.fromCharCode(160), "g");
            clone.text( clone.text().replace(re, "") );
            fields_left.append(clone);
        });
        e.preventDefault();
    });

    /**
     * Remove categories event
     */
    remove_fields.click(function(e){
        var selected_fields = fields_left.find("option:selected");
        selected_fields.each(function(key, option){
            $(option).remove();
        });
        e.preventDefault();
    });

    /**
     * select the items after they have been removed
     */
    $(document).on('submit', '#storereservation_admin_form', function(e) {
        $('select#items').find('option').attr('selected', 'selected');
    });
});
$(function(){
    var cats_left = $("#fields"),
        cats_right = $("#availableFields"),
        remove_cats = $("#removeField"),
        add_cats = $("#addField");

    /**
     * ADD fields event
     */
    add_cats.click(function(e){
        var selected_items = cats_right.find("option:selected");
        selected_items.each(function(key, option){
            var clone = $(option).clone();
            var re = new RegExp(String.fromCharCode(160), "g");
            clone.text( clone.text().replace(re, "") );
            cats_left.append(clone);
        });
        e.preventDefault();
    });
    /**
     * REMOVE fields event
     */
    remove_cats.click(function(e){
        var selected_items = cats_left.find("option:selected");
        selected_items.each(function(key, option){
            $(option).remove();
        });
        setTimeout(function(){
            $('#fields option').each(function(){ $(this).attr('selected','selected');});
        },100);
        e.preventDefault();
    });
    $(document).on('click', '#module_form_submit_btn', function(){
        $('#fields option').each(function(){ $(this).attr('selected','selected');});
        console.log('aa');
        setTimeout(function(){
            $('#storereservation_admin_form').submit();
        },500);

    });
});
