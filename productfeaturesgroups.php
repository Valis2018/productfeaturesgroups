<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (! defined('_PS_VERSION_')) {
    exit;
}

class Productfeaturesgroups extends Module
{
    /**
     * Module configuration keys and their default value.
     * @var array
     */
    protected $configs = array(
        'PFG_GROUP_DESCRIPTION' => '#7a7a7a',
        'PFG_FEATURE_DETAIL' => '#7a7a7a',
        'PFG_FEATURE_BACKGROUND' => '#f6f6f6',
        'PFG_FEATURE' => '#7a7a7a',
        'PFG_GROUP_TITLE' => '#7a7a7a',
        'PFG_GROUP_BACKGROUND' => '#f2f4f7',
    );

    /**
     * Hooks used by this module.
     * @var array
     */
    protected $hooks = array(
        '1.7' => array(
            'displayFooterProduct',
            'header',
            'displayBackOfficeHeader',
        ),
        '1.6' => array(
            'displayFooterProduct',
            'displayBackOfficeHeader',
            'header',
        ),
    );


    /**
     * Create a new module instance.
     * @return void
     */
    public function __construct()
    {
        $this->name = 'productfeaturesgroups';
        $this->tab = 'front_office_features';
        $this->author = 'Active Design';
        $this->version = '1.0.2';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->module_key = '07bf3a856f4ba5a4fc21dfb0d63113d0';
        $this->author_address = '0xc0D7cE57752e47305707d7174B9686C0Afb229c3';

        parent::__construct();
        $this->displayName = $this->l('Product Feature Groups PRO');
        $this->description = $this->l('Now you can add feature groups to your products efficiently');
        $this->ps_version_compliancy = array('min' => '1.6.0', 'max' => _PS_VERSION_);
    }

    /**
     * Install the module.
     * @return bool
     */
    public function install()
    {
        $installed = include_once(dirname(__FILE__).'/sql/install.php');

        return $installed &&
            parent::install() &&
            $this->registerHooks() &&
            $this->setConfigs() &&
            /**
             * install registered admin controllers
             */
            $this->createAdminControllers();
    }
    /**
     * Uninstall the module.
     * @return bool
     */
    public function uninstall()
    {
        $uninstall = include_once(dirname(__FILE__).'/sql/uninstall.php');
        return $uninstall &&
            parent::uninstall() &&
            $this->unregisterHooks() &&
            $this->unsetConfigs() &&
            // uninstall registered admin controllers
            $this->destroyAdminControllers();
    }
    /**
     * Uninstalling and then re-install the module.
     * @return bool
     */
    public function reset()
    {
        return $this->uninstall() &&
            $this->install();
    }

    /**
     * Register the module hooks.
     * @return bool
     */
    protected function registerHooks()
    {
        Tools::version_compare(_PS_VERSION_, '1.7', '>=') ? $hooks = $this->hooks['1.7'] : $hooks = $this->hooks['1.6'];

        foreach ($hooks as $hook) {
            if (! $this->registerHook($hook)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Set the module configuration.
     * @return bool
     */
    protected function setConfigs()
    {
        foreach ($this->configs as $config => $value) {
            if (! Configuration::updateValue($config, $value)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Unregister the module hooks.
     * @return bool
     */
    protected function unregisterHooks()
    {
        Tools::version_compare(_PS_VERSION_, '1.7', '>=')
            ? $hooks = $this->hooks['1.7']
            : $hooks = $this->hooks['1.6'];

        foreach ($hooks as $hook) {
            if (! $this->unregisterHook($hook)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Unset the module configuration.
     * @return bool
     */
    protected function unsetConfigs()
    {
        foreach (array_keys($this->configs) as $config) {
            if (! Configuration::deleteByName($config)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Render the module content.
     * @return void
     */
    public function getContent()
    {
        Tools::redirectAdmin($this->context->link->getAdminLink('AdminProductDetailsList', true));
    }

    public function hookBackOfficeHeader()
    {
        if (isset($this->context->controller->module) &&
            $this->context->controller->module instanceof Productfeaturesgroups) {
            $this->context->controller->addJquery();
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
        }
    }

    /**
     * @param bool $withToken
     * @return string
     * get the link to the core of the module
     */
    protected function getIndex($withToken = true)
    {
        return $this->context->link->getAdminLink('AdminModules', $withToken)
            .'&configure='.$this->name
            .'&tab_module='.$this->tab
            .'&module_name='.$this->name;
    }

    /**
     * @return array
     */
    public function assignConfigureLinks()
    {
        $id_product = Tools::getValue('id_product');
        $product_link = Tools::version_compare(_PS_VERSION_, '1.7', '>=')
            ? ($id_product ? $this->context->link->getAdminLink('AdminProducts', true, compact('id_product')) : null)
            : ($id_product ? $this->context->link->getAdminLink('AdminProducts', true).
                '&id_product='.$id_product.'&updateproduct' : null);
        return array(
            'pfg_documentation' => $this->context->link->getAdminLink('AdminModules')
                                    .'&configure='.$this->name
                                    .'&tab_module='.$this->tab
                                    .'&module_name='.$this->name,
            'pfg_back_to_catalog' => $this->context->link->getAdminLink('AdminProducts', true),
            'pfg_back_to_product' => $product_link,
            'pfg_colors' => $this->context->link->getAdminLink('AdminColorSelect')
        );
    }

    /**
     * Custom "back-office" admin controllers configs.
     * @var array
     */
    protected $admin_controllers = array(
        array(
            'name' => 'Group List',
            'class_name' => 'AdminProductDetailsList',
            'id_parent' => -1,
            'icon' => 'cogs',
        ),
        array(
            'name' => 'Color List',
            'class_name' => 'AdminColorSelect',
            'id_parent' => -1,
            'icon' => 'icon-tint',
        ),
    );

    /**
     * Add additional CSS/JS files when in the front-office.
     * @param  array $params
     * @return void
     */
    public function hookHeader($params)
    {
        $this->context->controller->addCSS($this->getPathUri().'views/css/front.css');
        $this->context->controller->addJS($this->getPathUri().'views/js/front.js');
    }

    /**
     * Create and save required admin controllers
     * @return bool
     */
    protected function createAdminControllers()
    {
        $max = count($languages);
        $languages = array_map('intval', Language::getIDs());

        foreach ($this->admin_controllers as $controller) {
            $tab = $this->getAdminControllerMock();

            foreach ($controller as $property => $value) {
                if ($property == 'name') {
                    $tab->$property = array_combine($languages, array_fill(0, $max, $value));
                } elseif ($property == 'id_parent' && is_string($value)) {
                    $tab->$property = (int)Tab::getIdFromClassName($value);
                } else {
                    $tab->$property = $value;
                }
            }

            if (! $tab->save()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Destroy admin controllers.
     * @return bool
     */
    protected function destroyAdminControllers()
    {
        $tabs = Tab::getCollectionFromModule($this->name);

        foreach ($tabs as $tab) {
            if (! $tab->delete()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get admin controller "tab" mock object
     * @return Tab
     */
    protected function getAdminControllerMock()
    {
        $tab = new Tab;
        $tab->position = 0;
        $tab->id_parent = 0;
        $tab->module = $this->name;
        return $tab;
    }

    /**
     * Get product details for the given product ID.
     * @param $idProduct
     * @return array
     * @throws PrestaShopDatabaseException
     */
    public function getProductDetails($idProduct)
    {
        $features = Product::getFrontFeaturesStatic((int)$this->context->language->id, $idProduct);
        $idLang = Context::getContext()->language->id;

        $groups = Db::getInstance()->executeS('
            SELECT gl.`name`, gl.`description`, g.`feature_ids` FROM `'._DB_PREFIX_.'pfg_group_lang` gl
            INNER JOIN `'._DB_PREFIX_.'pfg_group` g ON gl.id_group = g.id_group
            INNER JOIN `'._DB_PREFIX_.'pfg_group_product`ig ON gl.id_group = ig.id_group
            WHERE gl.`id_lang` = '. (int)$idLang .'
            AND ig.`id_product` = '. (int)$idProduct .'
            ORDER BY g.position');

        foreach ($groups as $key => $group) {
            $featureIds = array_map('intval', json_decode($group['feature_ids']));
            $filteredFeatures = array_filter($features, function ($feature) use ($featureIds) {
                return in_array((int)$feature['id_feature'], $featureIds);
            });

            $groups[$key]['features'] = $filteredFeatures;
        }

        return $groups;
    }

    /**
     * Assign color Configurations to smarty
     */
    public function getColors()
    {
        $this->context->smarty->assign(array(
            'PFG_GROUP_BACKGROUND' => Configuration::get('PFG_GROUP_BACKGROUND'),
            'PFG_GROUP_TITLE' => Configuration::get('PFG_GROUP_TITLE'),
            'PFG_FEATURE' => Configuration::get('PFG_FEATURE'),
            'PFG_GROUP_DESCRIPTION' => Configuration::get('PFG_GROUP_DESCRIPTION'),
            'PFG_FEATURE_BACKGROUND'=> Configuration::get('PFG_FEATURE_BACKGROUND'),
            'PFG_FEATURE_DETAIL' => Configuration::get('PFG_FEATURE_DETAIL'),
        ));
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookDisplayFooterProduct($params)
    {
        if (Tools::version_compare(_PS_VERSION_, '1.7', '>=')) {
            $groupsVars = $this->getProductDetails($params['product']['id_product']);
        } else {
            $groupsVars = $this->getProductDetails($params['product']->id);
        }
        $this->context->smarty->assign(
            array(
                '___groups'=> $groupsVars,
                'product' =>$params['product'],
                'prestashop17'=>Tools::version_compare(_PS_VERSION_, '1.7', '>='),
            )
        );

        $this->getColors();

        return $this->context->smarty->fetch($this->local_path . 'views/templates/front/front.tpl');
    }
}