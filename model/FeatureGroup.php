<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

class FeatureGroup extends ObjectModel
{
    public $name;
    public $description;
    public $position;
    public $feature_ids;
    public $id_group;

    /**
     * @var array
     */
    public static $definition = array (
        'table' => 'pfg_group',
        'primary' => 'id_group',
        'multilang' => true,
        'fields' => array (
            'name' => array(
                'type' => self::TYPE_STRING,
                'required' => true,
                'validate' => 'isString',
                'lang' => true,
                'size' => 255
            ),
            'description' => array(
                'type' => self::TYPE_STRING,
                'required' => false,
                'validate' => 'isString',
                'lang' => true,
                'size' => 255
            ),
            'position' => array(
                'type' => self::TYPE_INT,
                'required' => true,
                'validate' => 'isInt',
                'lang' => false,
                'size' => 11
            ),
            'feature_ids' => array(
                'type' => self::TYPE_STRING,
                'required' => true,
                'validate' => 'isAnything',
                'lang' => false,
            ),
        )
    );

    /**
     * @param bool $auto_date
     * @param bool $null_values
     * @return mixed
     */
    public function add($auto_date = true, $null_values = false)
    {
        if (is_array($this->feature_ids)) {
            $this->feature_ids = json_encode($this->feature_ids);
        }
        $id_product = (int)Tools::getValue('id_product');

        $this->id_product = $id_product;

        return parent::add($auto_date, $null_values);
    }

    public function update($null_values = false)
    {
        if (is_array($this->feature_ids)) {
            $this->feature_ids = json_encode($this->feature_ids);
        }
        return parent::update($null_values);
    }
}
